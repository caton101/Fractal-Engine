#   Fractal-Engine
#   Copyright (C) 2019  Cameron Himes

#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

#import the modules
import pygame
from pygame.locals import *
from time import sleep
import math

#settings
fractal_resolution = 4 #variable not yet used
fractal_zoom = 1
fractal_compression = 0.25
fractal_diverge_passes = 10
fractal_diverge_threshold = 700
screen_size_x = 1000
screen_size_y = 500
screen_alpha = 5000 #variable not yet used
old_max = 1000000
old_min = 0
new_max = 255
new_min = 0
debug_info = False

#reserve variables
board = []

#initalize pygame
pygame.init()
screen=pygame.display.set_mode((screen_size_x,screen_size_y),HWSURFACE|DOUBLEBUF|RESIZABLE)
pygame.display.flip()

#make system to color a pixel
def drawpixel(alpha,pos_x,pos_y):
    use_color = (alpha,alpha,alpha)
    pos = [pos_x,pos_y]
    pygame.draw.line(screen,use_color,pos,pos,1)

def compress(number):
    to_check = (((float(number) - old_min) * (new_max - new_min)) / (old_max - old_min)) + new_min
    # is the number too small?
    if to_check < 40:
        # compress the number then make it bigger
        return (((float(number) - old_min) * (new_max - new_min)) / (old_max - old_min)) + new_min * 10
    # is the number too big?
    elif to_check > 255:
        # make the color white
        return 255
    else:
        # the number is just fine, return the compressed value
        return to_check

def mendelbrot_test(screen_pos_x,screen_pos_y):
    iteration = 0
    #   (((OldValue - OldMin) * (NewMax - NewMin)) / (OldMax - OldMin)) + NewMin
    a = (((screen_pos_x - 0) * (2.5 - -2.5)) / (screen_size_x - 0)) + -2.5
    b = (((screen_pos_y - 0) * (2.5 - -2.5)) / (screen_size_y - 0)) + -2.5
    orig_a = a
    orig_b = b
    while iteration < fractal_diverge_passes:
        aa = a*a - b*b
        bb = 2 * a * b
        a = aa + orig_a
        b = bb + orig_b
        if debug_info:
            print(a+b * fractal_compression)
        if abs(a+b)* fractal_compression > fractal_diverge_threshold:
            return abs(a+b)
        iteration += 1
    return abs(a+b)

#generate data to draw
def gen_board():
    global board
    board = []
    for y in range(screen_size_y):
        board.append([])
        for x in range(screen_size_x):
            #test for divergence
            board[y].append(mendelbrot_test(x,y)) #returns z
    if debug_info:
       print(board)
       print("The highest number is %s." % (max(max(board))))

def draw():
    for y in range(len(board)):
        for x in range(len(board[y])):
            if board[y][x] < 100:
                drawpixel(0,x,y)
            else:
                drawpixel(math.floor(compress(board[y][x])),x,y)

def render():
    gen_board()
    draw()
    pygame.display.flip()

# force first render
render()
# only use the loop in pygame
while True:
    pygame.event.pump()
    event=pygame.event.wait()
    if event.type==QUIT:
        pygame.display.quit()
        print("[EVENT] Exiting program.")
        pygame.quit()
        break
    elif event.type==VIDEORESIZE:
        screen=pygame.display.set_mode(event.dict['size'],HWSURFACE|DOUBLEBUF|RESIZABLE)
        print("[EVENT] Screen resizing to %s x %s..." % (event.dict['size'][0],event.dict['size'][1]))
        screen_size_x = event.dict['size'][0]
        screen_size_y = event.dict['size'][1]
        print("[EVENT] Rendering Fractal...")
        render()
        print("[EVENT] Rendering Complete.")
exit()
